package com.psawesome.ch1.client;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.Charset;
import java.util.logging.Logger;

public class EchoClientHandler extends ChannelInboundHandlerAdapter {
    Logger log = Logger.getLogger("EchoClientHandler");

    @Override     //최초 활성화 되었을 때 실행된다.
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        String s = "Hello, Netty";

        ByteBuf messageBuffer = Unpooled.buffer();
        messageBuffer.writeBytes(s.getBytes());

        var sb = new StringBuilder();
        sb.append("전송 문자열 [");
        sb.append(s);
        sb.append("]");

        log.info(sb.toString());

        //채널에 기록하는 write, 채널에 기록된 데이터를 서버로 전송하는 flush 합친 것
        ctx.writeAndFlush(messageBuffer);
    }

    @Override    //서버로부터 수신된 데이터가 있을 때 호출되는 네티 이벤트
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //데이터를 msg 객체에서 문자열 데이터로 추출
        var readMessage = ((ByteBuf) msg).toString(Charset.defaultCharset());

        var sb = new StringBuilder();
        sb.append("수신 문자열 [");
        sb.append(readMessage);
        sb.append("]");

        log.info(sb.toString());
    }

    /*
        channelRead() -> channelReadComplete()
    */

    @Override // 데이터를 모두 읽었을 때 호출
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}

