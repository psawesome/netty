package com.psawesome.ch1.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EchoServer {

    public static void main(String[] args) {
        // write your code here
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {

            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new EchoServerHandler());
                        }
                    });

            new UserPub().userRun();

            ChannelFuture f = b.bind(8888).sync();
            f.channel().closeFuture().sync();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

}

class UserPub {
    Logger log = Logger.getLogger("TestLogger");
    public void userRun() throws InterruptedException {
        ExecutorService es = Executors.newSingleThreadExecutor();
        Iterable<Integer> collect = Stream.iterate(1, f -> f + 1).limit(100).collect(Collectors.toList());

        Flow.Publisher<Integer> pub = sub ->
                sub.onSubscribe(new Flow.Subscription() {
                    @Override
                    public void request(long l) {
                        es.submit(() -> {
                            try {
                                var i = 0;
                                Iterator<Integer> iterator = collect.iterator();
                                while (i++ < l) {
                                    if (iterator.hasNext()) sub.onNext(iterator.next());
                                    else {
                                        sub.onComplete();
                                        break;
                                    }
                                }
                            } catch (Exception e) {
                                sub.onError(e);
                            }
                        });
                    }

                    @Override
                    public void cancel() {
                    }
                });

        Flow.Subscriber<Integer> sub = new Flow.Subscriber<Integer>() {
            Flow.Subscription subscription;

            @Override
            public void onSubscribe(Flow.Subscription subscription) {
                this.subscription = subscription;
                this.subscription.request(100);

            }

            @Override
            public void onNext(Integer integer) {
                log.info(Thread.currentThread().getName() + ", value = " + integer);
                subscription.request(3);
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onComplete() {
                System.out.println("onComplete");
            }
        };

        pub.subscribe(sub);

        es.awaitTermination(1000L, TimeUnit.MILLISECONDS);
        es.shutdown();
    }
}
